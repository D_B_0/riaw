# Rust implementation of Peter Shirley's [Raytracing in a Weekend](https://raytracing.github.io)

Examples:
![](example_images/1.webp)
![](example_images/2.png)