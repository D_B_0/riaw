use std::{
    io::Write,
    time::{Duration, Instant},
};

pub struct ProgressBar {
    start_time: Instant,
    increment: f32,
    prev_progress: f32,
    prev_time: Instant,
    eta: Option<Duration>,
}

impl ProgressBar {
    pub fn new(num_chars: usize) -> Self {
        let now = Instant::now();
        Self {
            start_time: now,
            increment: 1.0 / num_chars as f32,
            prev_progress: 0.0,
            prev_time: now,
            eta: None,
        }
    }

    pub fn print_sequental(&mut self, progress: f32) {
        self.recalc_eta(progress);
        self.print(progress, &"\x1B[J", &"\r");
    }

    fn recalc_eta(&mut self, progress: f32) {
        let p = progress.max(0.0).min(1.0);
        let now = Instant::now();

        let speed = (p - self.prev_progress) / (now - self.prev_time).as_secs_f32();
        if speed.abs() > 1e-10 {
            self.eta = None;
            let time = (1.0 - p) / speed;
            self.eta = Some(Duration::from_secs_f32(time));
        } else {
            self.eta = None;
        }

        self.prev_progress = p;
        self.prev_time = now;
    }

    pub fn print_end(&mut self, progress: f32) {
        self.eta = None;
        self.print(progress, &"", &"\n");
    }

    fn print(&self, progress: f32, start_chars: &str, end_chars: &str) {
        let p = progress.max(0.0).min(1.0);
        print!(
            " {}{:.1}% [{}{}] Elapsed: {}, ETA: {}{}",
            start_chars,
            progress * 100.0,
            "=".repeat((p / self.increment).ceil() as usize),
            " ".repeat(
                (1.0 / self.increment).ceil() as usize - (p / self.increment).ceil() as usize
            ),
            format_duration(Instant::now() - self.start_time),
            if let Some(eta) = self.eta {
                format_duration(eta)
            } else {
                "NAN".to_owned()
            },
            end_chars,
        );
        let _ = std::io::stdout().flush();
    }
}

fn format_duration(d: Duration) -> String {
    if d.as_secs() < 60 * 60 {
        format!("{}:{:0>2}", d.as_secs() / 60, d.as_secs() % 60)
    } else {
        format!(
            "{}:{:0>2}:{:0>2}",
            d.as_secs() / 3600,
            (d.as_secs() % 3600) / 60,
            (d.as_secs() % 3600) % 60
        )
    }
}
