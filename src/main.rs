use std::num::NonZeroUsize;
use std::sync::{mpsc, Arc, Mutex};
use std::thread;

use clap::Parser;

mod lib;
use lib::*;
mod progress_bar;
use progress_bar::ProgressBar;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Path to the the file to save
    filepath: String,

    /// Numbers of sample per pixel. A higher number decreases the noisiness, but increases render time.
    /// When multithreading, if the number of samples per pixel is not divisible by the number of threads,
    /// it will be rounded down to the nearest integer multiple
    #[clap(long, default_value = "400")]
    samples: NonZeroUsize,

    /// Max number of times a ray can bounce before recursion is stopped. Coincides with the number of
    /// reflections seen in a mirror
    #[clap(long, default_value = "50")]
    max_bounces: usize,
}

fn main() -> std::io::Result<()> {
    let args: Args = Args::parse();

    let progress_arc = Arc::new(Mutex::new(0.0));

    let mut scene = Scene::final_scene(args.samples, args.max_bounces, Some(progress_arc.clone()));

    let (tx, rx) = mpsc::channel();

    let print_handle = {
        let progress_arc = progress_arc.clone();
        thread::spawn(move || {
            let mut progress_bar = ProgressBar::new(80);
            while rx.try_recv().is_err() {
                let progress = *progress_arc.lock().unwrap();
                progress_bar.print_sequental(progress);
                thread::sleep(std::time::Duration::from_secs(1));
            }
            progress_bar.print_end(1.0);
        })
    };

    let pix_colors = scene.render()?;

    let mut buffer = image::ImageBuffer::new(scene.get_width() as u32, scene.get_height() as u32);

    for (i, &color) in pix_colors.iter().enumerate() {
        buffer[(
            (i % scene.get_width()) as u32,
            (i / scene.get_width()) as u32,
        )] = image::Rgb([
            (color.x().clamp(0.0, 1.0) * 255.0) as u8,
            (color.y().clamp(0.0, 1.0) * 255.0) as u8,
            (color.z().clamp(0.0, 1.0) * 255.0) as u8,
        ]);
    }

    let _ = tx.send(());

    let ret = if let Err(_) = buffer.save(args.filepath) {
        Err(std::io::Error::new(
            std::io::ErrorKind::Other,
            "Failed to write to file",
        ))
    } else {
        Ok(())
    };

    print_handle.join().unwrap();

    ret
}
