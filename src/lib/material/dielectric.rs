use super::*;

pub struct Dielectric {
    tint: Vec3,
    ior: f64,
}

impl Dielectric {
    pub fn new(ior: f64) -> Self {
        Self {
            tint: Vec3::new(1.0, 1.0, 1.0),
            ior,
        }
    }

    fn reflectance(cos: f64, ref_idx: f64) -> f64 {
        let mut r0 = (1.0 - ref_idx) / (1.0 + ref_idx);
        r0 *= r0;
        r0 + (1.0 - r0) * (1.0 - cos).powi(5)
    }
}

impl Material for Dielectric {
    fn scatter(
        &self,
        r_in: &Ray,
        rec: &HitRecord,
        attenuation: &mut Vec3,
        scattered: &mut Ray,
    ) -> bool {
        *attenuation = self.tint;
        let ref_ratio = if rec.front_face {
            1.0 / self.ior
        } else {
            self.ior
        };

        let cos_theta = Vec3::dot(&-r_in.dir.normalized(), &rec.normal).min(1.0);
        let sin_theta = (1.0 - cos_theta).sqrt();

        let direction = if ref_ratio * sin_theta > 1.0
            || Dielectric::reflectance(cos_theta, ref_ratio) > rand_norm()
        {
            Vec3::reflect(r_in.dir.normalized(), rec.normal)
        } else {
            Vec3::refract(r_in.dir.normalized(), rec.normal, ref_ratio)
        };

        *scattered = Ray::with_time(rec.point, direction, r_in.time);

        true
    }
}
