use super::*;

pub mod lambertian;
pub use lambertian::*;
pub mod metal;
pub use metal::*;
pub mod dielectric;
pub use dielectric::*;
pub mod diffuse_light;
pub use diffuse_light::*;
pub mod isotropic;
pub use isotropic::*;

pub trait Material: Sync + Send {
    fn scatter(
        &self,
        r_in: &Ray,
        rec: &HitRecord,
        attenuation: &mut Vec3,
        scattered: &mut Ray,
    ) -> bool;

    fn emitted(&self, _u: f64, _v: f64, _p: &Vec3) -> Vec3 {
        Vec3::zero()
    }
}
