use super::*;

pub struct DiffuseLight {
    pub emit: Arc<dyn Texture>,
}

#[allow(dead_code)]
impl DiffuseLight {
    pub fn new(emit: Vec3) -> Self {
        Self {
            emit: Arc::new(texture::SolidColor::new(emit)),
        }
    }

    pub fn from_texture(emit: Arc<dyn Texture>) -> Self {
        Self { emit }
    }
}

impl Material for DiffuseLight {
    fn scatter(
        &self,
        _r_in: &Ray,
        _rec: &HitRecord,
        _attenuation: &mut Vec3,
        _scattered: &mut Ray,
    ) -> bool {
        false
    }

    fn emitted(&self, u: f64, v: f64, p: &Vec3) -> Vec3 {
        self.emit.value(u, v, p)
    }
}
