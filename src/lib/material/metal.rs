use super::*;

pub struct Metal {
    albedo: Arc<dyn Texture>,
    fuzz: f64,
}

#[allow(dead_code)]
impl Metal {
    pub fn new(albedo: Vec3, fuzz: f64) -> Self {
        Self {
            albedo: Arc::new(texture::SolidColor::new(albedo)),
            fuzz: fuzz.clamp(0.0, 1.0),
        }
    }

    pub fn from_texture(albedo: Arc<dyn Texture>, fuzz: f64) -> Self {
        Self {
            albedo,
            fuzz: fuzz.clamp(0.0, 1.0),
        }
    }
}

impl Material for Metal {
    fn scatter(
        &self,
        r_in: &Ray,
        rec: &HitRecord,
        attenuation: &mut Vec3,
        scattered: &mut Ray,
    ) -> bool {
        let reflected = Vec3::reflect(r_in.dir.normalized(), rec.normal);
        *scattered = Ray::with_time(
            rec.point,
            reflected + self.fuzz * Vec3::random_in_unit_sphere(),
            r_in.time,
        );
        *attenuation = self.albedo.value(rec.u, rec.v, &rec.point);

        Vec3::dot(&reflected, &rec.normal) > 0.0
    }
}
