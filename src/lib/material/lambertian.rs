use super::*;

pub struct Lambertian {
    pub albedo: Arc<dyn Texture>,
}

#[allow(dead_code)]
impl Lambertian {
    pub fn new(albedo: Vec3) -> Self {
        Self {
            albedo: Arc::new(texture::SolidColor::new(albedo)),
        }
    }

    pub fn from_texture(albedo: Arc<dyn Texture>) -> Self {
        Self { albedo }
    }
}

impl Material for Lambertian {
    fn scatter(
        &self,
        r_in: &Ray,
        rec: &HitRecord,
        attenuation: &mut Vec3,
        scattered: &mut Ray,
    ) -> bool {
        let mut scatter_direction = rec.normal + Vec3::random_on_unit_sphere();

        if scatter_direction.near_zero() {
            scatter_direction = rec.normal;
        }
        *scattered = Ray::with_time(rec.point, scatter_direction, r_in.time);
        *attenuation = self.albedo.value(rec.u, rec.v, &rec.point);

        true
    }
}
