use crate::lib::texture::SolidColor;

use super::*;

pub struct Isotropic {
    color: Arc<dyn Texture>,
}

#[allow(dead_code)]
impl Isotropic {
    pub fn new(color: Vec3) -> Self {
        Self {
            color: Arc::new(SolidColor::new(color)),
        }
    }

    pub fn from_texture(color: Arc<dyn Texture>) -> Self {
        Self { color: color }
    }
}

impl Material for Isotropic {
    fn scatter(
        &self,
        r_in: &Ray,
        rec: &HitRecord,
        attenuation: &mut Vec3,
        scattered: &mut Ray,
    ) -> bool {
        *scattered = Ray::with_time(rec.point, Vec3::random_in_unit_sphere(), r_in.time);
        *attenuation = self.color.value(rec.u, rec.v, &rec.point);

        true
    }
}
