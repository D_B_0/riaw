use super::*;

#[derive(Clone, Copy)]
pub struct Ray {
    pub orig: Vec3,
    pub dir: Vec3,
    pub time: f64,
}

#[allow(dead_code)]
impl Ray {
    pub fn new(orig: Vec3, dir: Vec3) -> Self {
        Self {
            orig,
            dir: dir.normalized(),
            time: 0.0,
        }
    }

    pub fn with_time(orig: Vec3, dir: Vec3, time: f64) -> Self {
        Self {
            orig,
            dir: dir.normalized(),
            time,
        }
    }

    pub fn default() -> Self {
        Self {
            orig: Vec3::zero(),
            dir: Vec3::zero(),
            time: 0.0,
        }
    }

    pub fn at(&self, t: f64) -> Vec3 {
        self.orig + t * self.dir
    }
}
