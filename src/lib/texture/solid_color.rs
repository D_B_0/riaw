use super::*;

pub struct SolidColor {
    value: Vec3,
}

#[allow(dead_code)]
impl SolidColor {
    pub fn new(value: Vec3) -> Self {
        Self { value }
    }
}

impl Texture for SolidColor {
    fn value(&self, _u: f64, _v: f64, _p: &Vec3) -> Vec3 {
        self.value
    }
}
