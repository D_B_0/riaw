use super::*;

pub struct Checker {
    odd: Arc<dyn Texture>,
    even: Arc<dyn Texture>,
    scale: f64,
}

#[allow(dead_code)]
impl Checker {
    pub fn new(odd: Arc<dyn Texture>, even: Arc<dyn Texture>, scale: f64) -> Self {
        Self { odd, even, scale }
    }

    pub fn from_colors(odd: Vec3, even: Vec3, scale: f64) -> Self {
        Self {
            odd: Arc::new(texture::SolidColor::new(odd)),
            even: Arc::new(texture::SolidColor::new(even)),
            scale,
        }
    }
}

impl Texture for Checker {
    fn value(&self, u: f64, v: f64, p: &Vec3) -> Vec3 {
        let sin_x = (self.scale * p.x()).sin().signum();
        let sin_y = (self.scale * p.y()).sin().signum();
        let sin_z = (self.scale * p.z()).sin().signum();
        if sin_x * sin_y * sin_z > 0.0 {
            self.even.value(u, v, p)
        } else {
            self.odd.value(u, v, p)
        }
    }
}
