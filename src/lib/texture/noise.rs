use super::*;

pub struct Noise {
    noise: Perlin,
    scale: f64,
    iterations: Option<usize>,
}

#[allow(dead_code)]
impl Noise {
    pub fn new(scale: f64) -> Self {
        Self {
            noise: Perlin::new(),
            scale,
            iterations: None,
        }
    }

    pub fn with_iterations(scale: f64, iterations: usize) -> Self {
        Self {
            noise: Perlin::new(),
            scale,
            iterations: Some(iterations),
        }
    }
}

impl Texture for Noise {
    fn value(&self, _u: f64, _v: f64, p: &Vec3) -> Vec3 {
        self.noise
            .turbulence(&(*p * self.scale), self.iterations)
            .map(-1.0, 1.0, 0.0, 1.0)
            * Vec3::new(1.0, 1.0, 1.0)
        // (p.z() * self.scale + 10.0 * self.noise.turbulence(p, None).abs())
        //     .sin()
        //     .map(-1.0, 1.0, 0.0, 1.0)
        //     * Vec3::new(1.0, 1.0, 1.0)
    }
}
