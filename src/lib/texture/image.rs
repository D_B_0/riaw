use std::path::Path;

use ::image::{io::Reader as ImageReader, DynamicImage, GenericImageView};

use super::*;

pub struct Image {
    img: DynamicImage,
}

#[allow(dead_code)]
impl Image {
    pub fn new<P>(filename: P) -> Self
    where
        P: AsRef<Path>,
    {
        if let Ok(img) = ImageReader::open(filename.as_ref()) {
            if let Ok(img) = img.decode() {
                return Self { img };
            }
        }
        panic!("Unable to open image at {}", filename.as_ref().display())
    }

    pub fn width(&self) -> u32 {
        self.img.dimensions().0
    }

    pub fn height(&self) -> u32 {
        self.img.dimensions().1
    }
}

impl Texture for Image {
    fn value(&self, u: f64, v: f64, _p: &Vec3) -> Vec3 {
        let u = u.clamp(0.0, 1.0);
        let v = 1.0 - v.clamp(0.0, 1.0);

        let mut i = (u * self.width() as f64) as u32;
        let mut j = (v * self.height() as f64) as u32;

        if i >= self.width() {
            i = self.width() - 1;
        }
        if j >= self.height() {
            j = self.height() - 1;
        }

        let pix = self.img.get_pixel(i, j);
        Vec3::new(pix.0[0] as f64, pix.0[1] as f64, pix.0[2] as f64) / 255.0
    }
}
