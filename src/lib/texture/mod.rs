use super::*;

pub mod solid_color;
pub use solid_color::*;
pub mod checker;
pub use checker::*;
pub mod noise;
pub use noise::*;
pub mod image;
pub use self::image::*;

pub trait Texture: Sync + Send {
    fn value(&self, u: f64, v: f64, p: &Vec3) -> Vec3;
}
