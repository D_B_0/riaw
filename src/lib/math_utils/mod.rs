pub use super::*;

pub mod vec3;
pub use vec3::*;
pub mod random;
pub use random::{rand_int, rand_norm, rand_range, Perlin};

pub trait Map {
    fn map(&self, prev_min: Self, prev_max: Self, next_min: Self, next_max: Self) -> Self;
}

impl Map for f64 {
    fn map(&self, prev_min: Self, prev_max: Self, next_min: Self, next_max: Self) -> Self {
        next_min + (*self - prev_min) * ((next_max - next_min) / (prev_max - prev_min))
    }
}
