pub mod perlin;
pub use perlin::Perlin;

use super::*;

use rand::Rng;

pub fn rand_norm() -> f64 {
    rand::random::<f64>()
}

pub fn rand_range(min: f64, max: f64) -> f64 {
    rand_norm() * (max - min) + min
}

pub fn rand_int(min: i64, max: i64) -> i64 {
    rand::thread_rng().gen_range(min..(max + 1))
}
