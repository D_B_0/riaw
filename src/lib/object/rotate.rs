use super::*;

pub enum Axis {
    X,
    Y,
    Z,
}

pub struct Rotate {
    obj: Arc<dyn Hittable>,
    axis: Axis,
    sin: f64,
    cos: f64,
    b_box: Option<AABB>,
}

// pub struct RotateY {
//     obj: Arc<dyn Hittable>,
//     sin: f64,
//     cos: f64,
//     b_box: Option<AABB>,
// }

#[allow(dead_code)]
impl Rotate {
    pub fn new(theta: f64, axis: Axis, obj: Arc<dyn Hittable>) -> Self {
        let sin = theta.sin();
        let cos = theta.cos();

        let b_box = if let Some(b_box) = obj.bounding_box(0.0, 1.0) {
            let mut min = Vec3::new(f64::INFINITY, f64::INFINITY, f64::INFINITY);
            let mut max = -Vec3::new(f64::INFINITY, f64::INFINITY, f64::INFINITY);

            for i in 0..2 {
                for j in 0..2 {
                    for k in 0..2 {
                        let x = i as f64 * b_box.maximum.x() + (1.0 - i as f64) * b_box.minimum.x();
                        let y = j as f64 * b_box.maximum.y() + (1.0 - j as f64) * b_box.minimum.y();
                        let z = k as f64 * b_box.maximum.z() + (1.0 - k as f64) * b_box.minimum.z();

                        let (new_x, new_y, new_z) = match axis {
                            Axis::X => (x, -sin * z + cos * y, cos * z + sin * y),
                            Axis::Y => (cos * x + sin * z, y, -sin * x + cos * z),
                            Axis::Z => (cos * x + sin * y, -sin * x + cos * y, z),
                        };

                        let tester = Vec3::new(new_x, new_y, new_z);

                        for c in 0..3 {
                            min[c] = min[c].min(tester[c]);
                            max[c] = max[c].max(tester[c]);
                        }
                    }
                }
            }
            Some(AABB::new(min, max))
        } else {
            None
        };

        Self {
            obj: obj.clone(),
            sin,
            cos,
            axis,
            b_box,
        }
    }

    pub fn x(theta: f64, obj: Arc<dyn Hittable>) -> Self {
        Self::new(theta, Axis::X, obj)
    }
    pub fn y(theta: f64, obj: Arc<dyn Hittable>) -> Self {
        Self::new(theta, Axis::Y, obj)
    }
    pub fn z(theta: f64, obj: Arc<dyn Hittable>) -> Self {
        Self::new(theta, Axis::Z, obj)
    }
}

impl Hittable for Rotate {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        let mut origin = (*r).orig;
        let mut direction = (*r).dir;

        let (component_1_index, component_2_index) = match self.axis {
            Axis::X => (2, 1),
            Axis::Y => (0, 2),
            Axis::Z => (0, 1),
        };
        origin[component_1_index] =
            self.cos * r.orig[component_1_index] - self.sin * r.orig[component_2_index];
        origin[component_2_index] =
            self.sin * r.orig[component_1_index] + self.cos * r.orig[component_2_index];

        direction[component_1_index] =
            self.cos * r.dir[component_1_index] - self.sin * r.dir[component_2_index];
        direction[component_2_index] =
            self.sin * r.dir[component_1_index] + self.cos * r.dir[component_2_index];

        let rot_ray = Ray::with_time(origin, direction, r.time);

        if let Some(mut rec) = self.obj.hit(&rot_ray, t_min, t_max) {
            let mut p = rec.point;
            let mut normal = rec.normal;

            p[component_1_index] =
                self.cos * rec.point[component_1_index] + self.sin * rec.point[component_2_index];
            p[component_2_index] =
                -self.sin * rec.point[component_1_index] + self.cos * rec.point[component_2_index];

            normal[component_1_index] =
                self.cos * rec.normal[component_1_index] + self.sin * rec.normal[component_2_index];
            normal[component_2_index] = -self.sin * rec.normal[component_1_index]
                + self.cos * rec.normal[component_2_index];

            rec.point = p;
            rec.set_face_normal(&rot_ray, &normal);
            Some(rec)
        } else {
            None
        }
    }

    fn bounding_box(&self, _time0: f64, _time1: f64) -> Option<AABB> {
        self.b_box
    }

    fn insert(self, world: &mut HittableList) {
        world.add(Arc::new(self))
    }
}
