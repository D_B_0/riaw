use super::*;

use std::sync::Arc;

pub struct Cylinder {
    objs: object::HittableList,

    // Needed for bounding box calculation
    center: Vec3,
    // direction: Vec3,
    radius: f64,
    length: f64,
}

// Having run some tests with `hyperfine` it seems that
// `Cylinder` is less performant than two `Discs` and an `UncappedCylinder`
// on the same scene:
// {
//     let camera = Camera::new(
//         Vec3::zero(),
//         Vec3::new(0.0, 0.0, -1.0),
//         Vec3::new(0.0, 1.0, 0.0),
//         60.0,
//         1.0,
//         0.0,
//         1.0,
//     );

//     let mut world = object::HittableList::new();

//     let mat = Arc::new(material::Lambertian::new(Vec3::new(0.5, 0.5, 0.5)));

//     // add to `world` a cylinder with:
//     //    center: Vec3::new(0.0, 0.0, -5.0),
//     //    direction: Vec3::new(3.0, 1.0, 3.0),
//     //    radius: 0.5,
//     //    length: 2.0,

//     Scene::new(
//         Arc::new(world),
//         camera,
//         Settings::builder()
//             .width(400)
//             .aspect_ratio(1.0)
//             .samples(samples)
//             .max_bounces(max_bounces)
//             .build()
//             .unwrap(),
//         progress_arc,
//     )
// }
// with the opprtune changes to what is added to `world`

// the scene with the uncapped cylinder and discs (4.910s) ran 1.16
// times faster than the one with the cylinder (4.216s) over 100 runs

// This difference isn't a deal-breaker, but should be considered nonetheless
#[allow(dead_code)]
impl Cylinder {
    pub fn new(
        center: Vec3,
        direction: Vec3,
        radius: f64,
        length: f64,
        material: Arc<dyn Material>,
    ) -> Self {
        let mut objs = HittableList::new();
        objs.add(Arc::new(object::UncappedCylinder::new(
            center,
            direction.normalized(),
            radius,
            length,
            material.clone(),
        )));
        objs.add(Arc::new(object::Ring::disc(
            center + direction.normalized() * length / 2.0,
            direction.normalized(),
            radius,
            material.clone(),
        )));
        objs.add(Arc::new(object::Ring::disc(
            center - direction.normalized() * length / 2.0,
            -direction.normalized(),
            radius,
            material.clone(),
        )));
        Self {
            objs,
            center,
            // direction: Vec3,
            radius,
            length,
        }
    }
}

impl Hittable for Cylinder {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        self.objs.hit(r, t_min, t_max)
    }

    fn insert(self, world: &mut HittableList) {
        self.objs.insert(world);
    }

    fn bounding_box(&self, _time0: f64, _time1: f64) -> Option<AABB> {
        let encasing_radius = (self.length.powi(2) + self.radius.powi(2)).sqrt();
        Some(AABB::new(
            self.center - Vec3::new(encasing_radius, encasing_radius, encasing_radius),
            self.center + Vec3::new(encasing_radius, encasing_radius, encasing_radius),
        ))
    }
}
