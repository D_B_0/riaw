use std::sync::Arc;

use super::*;

pub struct Plane {
    pub point: Vec3,
    pub normal: Vec3,
    pub material: Arc<dyn Material>,
}

impl Plane {
    #[allow(dead_code)]
    pub fn new(point: Vec3, normal: Vec3, material: Arc<dyn Material>) -> Self {
        Self {
            point,
            normal,
            material,
        }
    }
}

impl Hittable for Plane {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        let num = Vec3::dot(&(self.point - r.orig), &self.normal);
        let den = Vec3::dot(&r.dir, &self.normal);
        if den.abs() < 1e-10 {
            return None;
        }

        let t = num / den;
        if t < t_min || t > t_max {
            return None;
        }

        let mut rec = HitRecord::new();
        rec.t = t;
        rec.point = r.at(t);

        rec.set_face_normal(r, &self.normal);

        rec.material = Some(self.material.clone());

        Some(rec)
    }

    fn insert(self, world: &mut HittableList) {
        world.add(Arc::new(self));
    }
}
