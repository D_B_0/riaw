use super::*;

pub struct Block {
    block_min: Vec3,
    block_max: Vec3,
    sides: HittableList,
}

#[allow(dead_code)]
impl Block {
    pub fn new(center: Vec3, dimentions: Vec3, material: Arc<dyn Material>) -> Self {
        let block_min = center - dimentions / 2.0;
        let block_max = center + dimentions / 2.0;

        let mut sides = HittableList::new();

        // right
        object::YZRect::new(
            Vec3::new(block_max.x(), center.y(), center.z()),
            dimentions.z(),
            dimentions.y(),
            material.clone(),
        )
        .insert(&mut sides);
        // left
        object::YZRect::new(
            Vec3::new(block_min.x(), center.y(), center.z()),
            dimentions.z(),
            dimentions.y(),
            material.clone(),
        )
        .insert(&mut sides);

        // top
        object::XZRect::new(
            Vec3::new(center.x(), block_max.y(), center.z()),
            dimentions.x(),
            dimentions.z(),
            material.clone(),
        )
        .insert(&mut sides);
        // bottom
        object::XZRect::new(
            Vec3::new(center.x(), block_min.y(), center.z()),
            dimentions.x(),
            dimentions.z(),
            material.clone(),
        )
        .insert(&mut sides);

        // back
        object::XYRect::new(
            Vec3::new(center.x(), center.y(), block_min.z()),
            dimentions.x(),
            dimentions.y(),
            material.clone(),
        )
        .insert(&mut sides);
        // front
        object::XYRect::new(
            Vec3::new(center.x(), center.y(), block_max.z()),
            dimentions.x(),
            dimentions.y(),
            material.clone(),
        )
        .insert(&mut sides);

        Self {
            block_min,
            block_max,
            sides,
        }
    }

    pub fn from_extremes(min: Vec3, max: Vec3, material: Arc<dyn Material>) -> Self {
        Block::new((max + min) / 2.0, max - min, material)
    }
}

impl Hittable for Block {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        self.sides.hit(r, t_min, t_max)
    }

    fn insert(self, world: &mut HittableList) {
        self.sides.insert(world);
    }

    fn bounding_box(&self, _time0: f64, _time1: f64) -> Option<AABB> {
        Some(AABB::new(self.block_min, self.block_max))
    }
}
