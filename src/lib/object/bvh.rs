use super::*;

pub struct BvhNode {
    b_box: AABB,
    left: Arc<dyn Hittable>,
    right: Arc<dyn Hittable>,
}

#[allow(dead_code)]
impl BvhNode {
    pub fn new(
        src_objs: &mut Vec<Arc<dyn Hittable>>,
        start: usize,
        end: usize,
        time0: f64,
        time1: f64,
    ) -> Self {
        let axis = rand_int(0, 2);
        let comparator =
            |a: &Arc<dyn Hittable>, b: &Arc<dyn Hittable>| AABB::compare(a, b, axis as i32);

        let object_span = end - start;

        let (left, right) = if object_span == 1 {
            (src_objs[start].clone(), src_objs[start].clone())
        } else if object_span == 2 {
            if comparator(&src_objs[start], &src_objs[start + 1]).is_lt() {
                (src_objs[start].clone(), src_objs[start + 1].clone())
            } else {
                (src_objs[start + 1].clone(), src_objs[start].clone())
            }
        } else {
            (&mut src_objs[start..end]).sort_by(comparator);

            let mid = start + object_span / 2;
            (
                Arc::new(BvhNode::new(src_objs, start, mid, time0, time1)) as Arc<dyn Hittable>,
                Arc::new(BvhNode::new(src_objs, mid, end, time0, time1)) as Arc<dyn Hittable>,
            )
        };

        let box_left = left
            .bounding_box(time0, time1)
            .expect("No bounding box in BvhNode constructor");
        let box_right = right
            .bounding_box(time0, time1)
            .expect("No bounding box in BvhNode constructor");

        Self {
            b_box: AABB::surrounding_box(box_left, box_right),
            left,
            right,
        }
    }
}

impl Hittable for BvhNode {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        if !self.b_box.hit(r, t_min, t_max) {
            return None;
        }

        let hit_left = self.left.hit(r, t_min, t_max);
        let hit_right = self.right.hit(
            r,
            t_min,
            if hit_left.is_some() {
                hit_left.clone().unwrap().t
            } else {
                t_max
            },
        );
        if hit_right.is_some() {
            hit_right
        } else if hit_left.is_some() {
            hit_left
        } else {
            None
        }
    }

    fn bounding_box(&self, _time0: f64, _time1: f64) -> Option<AABB> {
        Some(self.b_box)
    }

    fn insert(self, world: &mut HittableList) {
        world.add(Arc::new(self));
    }
}
