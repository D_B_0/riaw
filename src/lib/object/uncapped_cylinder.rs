use super::*;

use std::sync::Arc;

pub struct UncappedCylinder {
    pub center: Vec3,
    pub direction: Vec3,
    pub radius: f64,
    pub length: f64,
    pub material: Arc<dyn Material>,
}

impl UncappedCylinder {
    pub fn new(
        center: Vec3,
        direction: Vec3,
        radius: f64,
        length: f64,
        material: Arc<dyn Material>,
    ) -> Self {
        Self {
            center,
            direction: direction.normalized(),
            radius,
            length,
            material,
        }
    }
}

impl Hittable for UncappedCylinder {
    #[allow(non_snake_case)]
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        let B = r.dir - Vec3::dot(&r.dir, &self.direction) * self.direction;
        let C = r.orig - self.center
            + self.direction * Vec3::dot(&(self.center - r.orig), &self.direction);

        let h = Vec3::dot(&B, &C);
        let a = B.length_squared();
        let c = C.length_squared() - self.radius.powi(2);

        let delta = h.powi(2) - a * c;

        if delta < 0.0 {
            return None;
        }

        let (t, point) = {
            let mut t = None;
            let mut point = None;
            for sign in [-1.0, 1.0] {
                let candidate_t = (-h + sign * delta.sqrt()) / a;
                let candidate_point = r.at(candidate_t);

                if candidate_t > t_min
                    && candidate_t < t_max
                    && Vec3::dot(&(candidate_point - self.center), &self.direction).abs()
                        < self.length / 2.0
                {
                    t = Some(candidate_t);
                    point = Some(candidate_point);
                    break;
                }
            }

            if t.is_none() || point.is_none() {
                return None;
            }

            (t.unwrap(), point.unwrap())
        };

        let norm = (point
            - self.center
            - Vec3::dot(&(point - self.center), &self.direction) * self.direction)
            .normalized();

        let mut rec = HitRecord::new();
        rec.t = t;
        rec.point = point;
        rec.set_face_normal(r, &norm);

        rec.material = Some(self.material.clone());

        Some(rec)
    }

    fn insert(self, world: &mut HittableList) {
        world.add(Arc::new(self));
    }

    fn bounding_box(&self, _time0: f64, _time1: f64) -> Option<AABB> {
        let encasing_radius = (self.length.powi(2) + self.radius.powi(2)).sqrt();
        Some(AABB::new(
            self.center - Vec3::new(encasing_radius, encasing_radius, encasing_radius),
            self.center + Vec3::new(encasing_radius, encasing_radius, encasing_radius),
        ))
    }
}
