use std::sync::Arc;

use super::*;

pub struct Sphere {
    pub center: Vec3,
    pub radius: f64,
    pub material: Arc<dyn Material>,
}

#[allow(dead_code)]
impl Sphere {
    pub fn new(center: Vec3, radius: f64, material: Arc<dyn Material>) -> Self {
        Self {
            center,
            radius,
            material,
        }
    }

    /// Accepts:
    ///   p: a given point on the sphere of radius one, centered at the origin.
    ///
    /// Returns:
    ///   a touple of (u, v) coordinates
    pub fn sphere_uv(p: &Vec3) -> (f64, f64) {
        (
            ((f64::atan2(-p.z(), p.x())) + std::f64::consts::PI) / std::f64::consts::TAU,
            f64::acos(-p.y()) / std::f64::consts::PI,
        )
    }
}

impl Hittable for Sphere {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        let oc = r.orig - self.center;
        let a = r.dir.length_squared();
        let half_b = Vec3::dot(&oc, &r.dir);
        let c = oc.length_squared() - self.radius * self.radius;
        let discriminant = half_b * half_b - a * c;
        if discriminant < 0.0 {
            return None;
        }
        let sqrtd = discriminant.sqrt();

        let mut root = (-half_b - sqrtd) / a;
        if root < t_min || root > t_max {
            root = (-half_b + sqrtd) / a;
            if root < t_min || root > t_max {
                return None;
            }
        }

        let point = r.at(root);
        let outward_normal = (point - self.center) / self.radius;

        let mut rec = HitRecord::new();
        rec.t = root;
        rec.point = point;

        rec.set_face_normal(r, &outward_normal);

        let (u, v) = Sphere::sphere_uv(&outward_normal);
        rec.u = u;
        rec.v = v;

        rec.material = Some(Arc::clone(&self.material));

        Some(rec)
    }

    fn insert(self, world: &mut HittableList) {
        world.add(Arc::new(self));
    }

    fn bounding_box(&self, _time0: f64, _time1: f64) -> Option<AABB> {
        Some(AABB::new(
            self.center - Vec3::new(self.radius, self.radius, self.radius),
            self.center + Vec3::new(self.radius, self.radius, self.radius),
        ))
    }
}
