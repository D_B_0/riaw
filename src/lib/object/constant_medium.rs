use super::*;

pub struct ConstantMedium {
    boundary: Arc<dyn Hittable>,
    phase_function: Arc<dyn Material>,
    neg_inv_density: f64,
}

#[allow(dead_code)]
impl ConstantMedium {
    pub fn new(boundary: Arc<dyn Hittable>, color: Vec3, density: f64) -> Self {
        Self {
            boundary,
            phase_function: Arc::new(material::Isotropic::new(color)),
            neg_inv_density: -1.0 / density,
        }
    }

    pub fn from_texture(
        boundary: Arc<dyn Hittable>,
        texture: Arc<dyn Texture>,
        density: f64,
    ) -> Self {
        Self {
            boundary,
            phase_function: Arc::new(material::Isotropic::from_texture(texture)),
            neg_inv_density: -1.0 / density,
        }
    }
}

impl Hittable for ConstantMedium {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        let enable_debug = false;
        let debug = enable_debug && rand_norm() < 0.00001;

        let hit1 = self.boundary.hit(r, -f64::INFINITY, f64::INFINITY);
        if hit1.is_none() {
            return None;
        }
        let mut hit1 = hit1.unwrap();
        let hit2 = self.boundary.hit(r, hit1.t + 0.001, f64::INFINITY);
        if hit2.is_none() {
            return None;
        }
        let mut hit2 = hit2.unwrap();

        hit1.t = hit1.t.max(t_min);
        hit2.t = hit2.t.min(t_max);

        if hit1.t >= hit2.t {
            return None;
        }
        hit1.t = hit1.t.max(0.0);

        if debug {
            println!("t_min: {}, t_max: {}", hit1.t, hit2.t);
        }

        let distance_inside_boundary = hit2.t - hit1.t;
        let hit_distance = self.neg_inv_density * rand_norm().log(std::f64::consts::E);

        if debug {
            dbg!(distance_inside_boundary, hit_distance);
        }

        if hit_distance > distance_inside_boundary {
            return None;
        }

        let mut rec = HitRecord::new();

        rec.t = hit1.t + hit_distance;
        rec.point = r.at(rec.t);

        if debug {
            println!(
                "hit_distance: {}\nt: {}\np: {}",
                hit_distance, rec.t, rec.point
            );
        }

        rec.material = Some(self.phase_function.clone());

        // this is arbitrary and shouldn't change much
        rec.normal = Vec3::new(1.0, 0.0, 0.0);
        rec.front_face = true;

        Some(rec)
    }

    fn bounding_box(&self, time0: f64, time1: f64) -> Option<AABB> {
        self.boundary.bounding_box(time0, time1)
    }

    fn insert(self, _world: &mut HittableList) {
        todo!()
    }
}
