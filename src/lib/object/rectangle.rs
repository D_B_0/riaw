use std::sync::Arc;

use super::*;

pub struct Rectangle {
    center: Vec3,
    normal: Vec3,
    length_direction: Vec3,
    length: f64,
    height: f64,
    material: Arc<dyn Material>,
}
// The current implementation is not very easy to use due to
// the restriction of `length_direction` to the plane perpendicular
// to `normal`. This may be changed to use the projection on the
// plane. I will think of this, as the result may lead to unintuitive
// code
#[allow(dead_code)]
impl Rectangle {
    pub fn new(
        center: Vec3,
        normal: Vec3,
        length_direction: Vec3,
        length: f64,
        height: f64,
        material: Arc<dyn Material>,
    ) -> Self {
        assert!(
            Vec3::dot(&normal, &length_direction) < 1e-10,
            "`normal` and `length_direction` should be perpendicular"
        );
        Self {
            center,
            normal: normal.normalized(),
            length_direction: length_direction.normalized(),
            length,
            height,
            material,
        }
    }

    pub fn square(
        center: Vec3,
        normal: Vec3,
        length_direction: Vec3,
        side_length: f64,
        material: Arc<dyn Material>,
    ) -> Self {
        assert!(
            Vec3::dot(&normal, &length_direction) < 1e-10,
            "`normal` and `length_direction` should be perpendicular"
        );
        Self {
            center,
            normal: normal.normalized(),
            length_direction: length_direction.normalized(),
            length: side_length,
            height: side_length,
            material,
        }
    }
}

impl Hittable for Rectangle {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        let num = Vec3::dot(&(self.center - r.orig), &self.normal);
        let den = Vec3::dot(&r.dir, &self.normal);
        if den.abs() < 1e-10 {
            return None;
        }

        let t = num / den;
        if t < t_min || t > t_max {
            return None;
        }

        let point = r.at(t);

        if Vec3::dot(&self.length_direction, &(point - self.center)).abs() > self.length {
            return None;
        }
        if Vec3::dot(
            &Vec3::cross(&self.length_direction, &self.normal),
            &(point - self.center),
        )
        .abs()
            > self.height
        {
            return None;
        }

        let mut rec = HitRecord::new();
        rec.t = t;
        rec.point = point;

        rec.set_face_normal(r, &self.normal);

        rec.material = Some(self.material.clone());

        Some(rec)
    }

    fn insert(self, world: &mut HittableList) {
        world.add(Arc::new(self));
    }

    fn bounding_box(&self, _time0: f64, _time1: f64) -> Option<AABB> {
        let height_direction = Vec3::cross(&self.length_direction, &self.normal);

        Some(AABB::from_points(&[
            self.center + self.length_direction * self.length + height_direction * self.height,
            self.center + self.length_direction * self.length - height_direction * self.height,
            self.center - self.length_direction * self.length + height_direction * self.height,
            self.center - self.length_direction * self.length - height_direction * self.height,
        ]))
    }
}
