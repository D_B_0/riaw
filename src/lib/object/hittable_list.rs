use super::*;

pub struct HittableList {
    pub objects: Vec<Arc<dyn Hittable>>,
}

impl HittableList {
    pub fn new() -> Self {
        Self { objects: vec![] }
    }

    #[allow(dead_code)]
    pub fn clear(&mut self) {
        self.objects.clear();
    }

    pub fn add(&mut self, obj: Arc<dyn Hittable>) {
        self.objects.push(obj);
    }
}

impl Hittable for HittableList {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        let mut ret_rec = None;
        let mut closest = t_max;

        for obj in &self.objects {
            if let Some(rec) = obj.hit(r, t_min, closest) {
                ret_rec = Some(rec.clone());
                closest = rec.t;
            }
        }

        ret_rec
    }

    fn insert(mut self, world: &mut HittableList) {
        world.objects.append(&mut self.objects);
    }

    fn bounding_box(&self, time0: f64, time1: f64) -> Option<AABB> {
        if self.objects.is_empty() {
            return None;
        }

        let mut output_box = None;

        for obj in &self.objects {
            let b = obj.bounding_box(time0, time1)?;
            output_box = Some(if output_box.is_none() {
                b
            } else {
                AABB::surrounding_box(output_box.unwrap(), b)
            });
        }

        output_box
    }
}
