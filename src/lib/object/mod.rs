// Primitives
pub mod sphere;
pub use sphere::*;
pub mod plane;
pub use plane::*;
pub mod aa_rect;
pub use aa_rect::*;
pub mod block;
pub use block::*;
pub mod uncapped_cylinder;
pub use uncapped_cylinder::*;
pub mod ring;
pub use ring::*;
pub mod rectangle;
pub use rectangle::*;
pub mod cylinder;
pub use cylinder::*;
pub mod moving_sphere;
pub use moving_sphere::*;
// Collections
pub mod hittable_list;
pub use hittable_list::*;
pub mod bvh;
pub use bvh::*;
// Transformations
pub mod translate;
pub use translate::*;
pub mod rotate;
pub use rotate::*;
// Volumes
pub mod constant_medium;
pub use constant_medium::*;

use std::sync::Arc;

use super::*;

#[derive(Clone)]
pub struct HitRecord {
    pub point: Vec3,
    pub normal: Vec3,
    pub t: f64,
    pub front_face: bool,
    pub u: f64,
    pub v: f64,
    pub material: Option<Arc<dyn Material>>,
}

impl HitRecord {
    pub fn new() -> Self {
        Self {
            point: Vec3::zero(),
            normal: Vec3::zero(),
            t: -1.0,
            front_face: true,
            u: 0.0,
            v: 0.0,
            material: None,
        }
    }

    pub fn set_face_normal(&mut self, r: &Ray, outward_normal: &Vec3) {
        self.front_face = Vec3::dot(&r.dir, &outward_normal) < 0.0;
        self.normal = if self.front_face {
            *outward_normal
        } else {
            -*outward_normal
        };
    }
}

pub trait Hittable: Sync + Send {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord>;
    fn bounding_box(&self, _time0: f64, _time1: f64) -> Option<AABB> {
        None
    }

    fn insert(self, world: &mut HittableList);
}
