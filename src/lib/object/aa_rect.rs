use std::sync::Arc;

use super::*;

pub struct XYRect {
    center: Vec3,
    width: f64,
    height: f64,
    material: Arc<dyn Material>,
}

#[allow(dead_code)]
impl XYRect {
    pub fn new(center: Vec3, width: f64, height: f64, material: Arc<dyn Material>) -> Self {
        Self {
            center,
            width,
            height,
            material,
        }
    }

    fn x_min(&self) -> f64 {
        self.center.x() - self.width / 2.0
    }

    fn x_max(&self) -> f64 {
        self.center.x() + self.width / 2.0
    }

    fn y_min(&self) -> f64 {
        self.center.y() - self.height / 2.0
    }

    fn y_max(&self) -> f64 {
        self.center.y() + self.height / 2.0
    }
}

impl Hittable for XYRect {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        if r.dir.z().abs() < 1e-10 {
            return None;
        }
        let t = (self.center.z() - r.orig.z()) / r.dir.z();

        if !(t_min..t_max).contains(&t) {
            return None;
        }

        let x = r.at(t).x();
        let y = r.at(t).y();

        if x < self.x_min() || x > self.x_max() || y < self.y_min() || y > self.y_max() {
            return None;
        }

        let mut rec = HitRecord::new();
        rec.t = t;
        rec.point = r.at(t);

        rec.u = (x - self.x_min()) / self.width;
        rec.v = (y - self.y_min()) / self.height;

        rec.set_face_normal(r, &Vec3::new(0.0, 0.0, 1.0));

        rec.material = Some(self.material.clone());

        Some(rec)
    }

    fn insert(self, world: &mut HittableList) {
        world.add(Arc::new(self));
    }

    fn bounding_box(&self, _time0: f64, _time1: f64) -> Option<AABB> {
        Some(AABB::new(
            self.center
                - Vec3::new(1.0, 0.0, 0.0) * self.width
                - Vec3::new(0.0, 1.0, 0.0) * self.height
                - Vec3::new(0.0, 0.0, 0.1),
            self.center
                + Vec3::new(1.0, 0.0, 0.0) * self.width
                + Vec3::new(0.0, 1.0, 0.0) * self.height
                + Vec3::new(0.0, 0.0, 0.1),
        ))
    }
}

pub struct XZRect {
    center: Vec3,
    width: f64,
    height: f64,
    material: Arc<dyn Material>,
}

#[allow(dead_code)]
impl XZRect {
    pub fn new(center: Vec3, width: f64, height: f64, material: Arc<dyn Material>) -> Self {
        Self {
            center,
            width,
            height,
            material,
        }
    }

    fn x_min(&self) -> f64 {
        self.center.x() - self.width / 2.0
    }

    fn x_max(&self) -> f64 {
        self.center.x() + self.width / 2.0
    }

    fn z_min(&self) -> f64 {
        self.center.z() - self.height / 2.0
    }

    fn z_max(&self) -> f64 {
        self.center.z() + self.height / 2.0
    }
}

impl Hittable for XZRect {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        if r.dir.y().abs() < 1e-10 {
            return None;
        }
        let t = (self.center.y() - r.orig.y()) / r.dir.y();

        if !(t_min..t_max).contains(&t) {
            return None;
        }

        let x = r.at(t).x();
        let z = r.at(t).z();

        if x < self.x_min() || x > self.x_max() || z < self.z_min() || z > self.z_max() {
            return None;
        }

        let mut rec = HitRecord::new();
        rec.t = t;
        rec.point = r.at(t);

        rec.u = (x - self.x_min()) / self.width;
        rec.v = (z - self.z_min()) / self.height;

        rec.set_face_normal(r, &Vec3::new(0.0, 1.0, 0.0));

        rec.material = Some(self.material.clone());

        Some(rec)
    }

    fn insert(self, world: &mut HittableList) {
        world.add(Arc::new(self));
    }

    fn bounding_box(&self, _time0: f64, _time1: f64) -> Option<AABB> {
        Some(AABB::new(
            self.center
                - Vec3::new(1.0, 0.0, 0.0) * self.width
                - Vec3::new(0.0, 0.0, 1.0) * self.height
                - Vec3::new(0.0, 0.1, 0.0),
            self.center
                + Vec3::new(1.0, 0.0, 0.0) * self.width
                + Vec3::new(0.0, 0.0, 1.0) * self.height
                + Vec3::new(0.0, 0.1, 0.0),
        ))
    }
}
pub struct YZRect {
    center: Vec3,
    width: f64,
    height: f64,
    material: Arc<dyn Material>,
}

#[allow(dead_code)]
impl YZRect {
    pub fn new(center: Vec3, width: f64, height: f64, material: Arc<dyn Material>) -> Self {
        Self {
            center,
            width,
            height,
            material,
        }
    }

    fn z_min(&self) -> f64 {
        self.center.z() - self.width / 2.0
    }

    fn z_max(&self) -> f64 {
        self.center.z() + self.width / 2.0
    }

    fn y_min(&self) -> f64 {
        self.center.y() - self.height / 2.0
    }

    fn y_max(&self) -> f64 {
        self.center.y() + self.height / 2.0
    }
}

impl Hittable for YZRect {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        if r.dir.x().abs() < 1e-10 {
            return None;
        }
        let t = (self.center.x() - r.orig.x()) / r.dir.x();

        if !(t_min..t_max).contains(&t) {
            return None;
        }

        let z = r.at(t).z();
        let y = r.at(t).y();

        if z < self.z_min() || z > self.z_max() || y < self.y_min() || y > self.y_max() {
            return None;
        }

        let mut rec = HitRecord::new();
        rec.t = t;
        rec.point = r.at(t);

        rec.u = (z - self.z_min()) / self.width;
        rec.v = (y - self.y_min()) / self.height;

        rec.set_face_normal(r, &Vec3::new(1.0, 0.0, 0.0));

        rec.material = Some(self.material.clone());

        Some(rec)
    }

    fn insert(self, world: &mut HittableList) {
        world.add(Arc::new(self));
    }

    fn bounding_box(&self, _time0: f64, _time1: f64) -> Option<AABB> {
        Some(AABB::new(
            self.center
                - Vec3::new(0.0, 0.0, 1.0) * self.width
                - Vec3::new(0.0, 1.0, 0.0) * self.height
                - Vec3::new(0.1, 0.0, 0.0),
            self.center
                + Vec3::new(0.0, 0.0, 1.0) * self.width
                + Vec3::new(0.0, 1.0, 0.0) * self.height
                + Vec3::new(0.1, 0.0, 0.0),
        ))
    }
}
