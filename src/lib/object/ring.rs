use std::sync::Arc;

use super::*;

pub struct Ring {
    pub center: Vec3,
    pub normal: Vec3,
    pub inner_radius: f64,
    pub outer_radius: f64,
    pub material: Arc<dyn Material>,
}

#[allow(dead_code)]
impl Ring {
    pub fn new(
        center: Vec3,
        normal: Vec3,
        outer_radius: f64,
        inner_radius: f64,
        material: Arc<dyn Material>,
    ) -> Self {
        Self {
            center,
            normal: normal.normalized(),
            inner_radius,
            outer_radius,
            material,
        }
    }
    pub fn disc(
        center: Vec3,
        normal: Vec3,
        outer_radius: f64,
        material: Arc<dyn Material>,
    ) -> Self {
        Self {
            center,
            normal: normal.normalized(),
            inner_radius: 0.0,
            outer_radius,
            material,
        }
    }
}

impl Hittable for Ring {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        let num = Vec3::dot(&(self.center - r.orig), &self.normal);
        let den = Vec3::dot(&r.dir, &self.normal);
        if den.abs() < 1e-10 {
            return None;
        }

        let t = num / den;
        if t < t_min || t > t_max {
            return None;
        }

        let point = r.at(t);

        if (point - self.center).length_squared() > self.outer_radius.powi(2)
            || (point - self.center).length_squared() < self.inner_radius.powi(2)
        {
            return None;
        }

        let mut rec = HitRecord::new();
        rec.t = t;
        rec.point = r.at(t);

        rec.set_face_normal(r, &self.normal);

        rec.material = Some(self.material.clone());

        Some(rec)
    }

    fn insert(self, world: &mut HittableList) {
        world.add(Arc::new(self));
    }

    fn bounding_box(&self, _time0: f64, _time1: f64) -> Option<AABB> {
        Some(AABB::new(
            self.center - Vec3::new(self.outer_radius, self.outer_radius, self.outer_radius),
            self.center + Vec3::new(self.outer_radius, self.outer_radius, self.outer_radius),
        ))
    }
}
