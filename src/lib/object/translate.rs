use super::*;

pub struct Translate {
    offset: Vec3,
    object: Arc<dyn Hittable>,
}

#[allow(dead_code)]
impl Translate {
    pub fn new(offset: Vec3, object: Arc<dyn Hittable>) -> Self {
        Self { offset, object }
    }
}

impl Hittable for Translate {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        let mut translated_r = *r;
        translated_r.orig -= self.offset;
        if let Some(mut rec) = self.object.hit(&translated_r, t_min, t_max) {
            rec.point += self.offset;
            rec.set_face_normal(&translated_r, &rec.normal.clone());
            return Some(rec);
        }
        None
    }

    fn bounding_box(&self, time0: f64, time1: f64) -> Option<AABB> {
        if let Some(b) = self.object.bounding_box(time0, time1) {
            Some(AABB::new(b.minimum + self.offset, b.maximum + self.offset))
        } else {
            None
        }
    }

    fn insert(self, world: &mut HittableList) {
        world.add(Arc::new(self));
    }
}
