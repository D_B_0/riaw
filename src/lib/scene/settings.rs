use super::*;

#[derive(Clone, Copy)]
pub struct Settings {
    pub width: usize,
    pub height: usize,
    pub aspect_ratio: f32,
    pub samples: NonZeroUsize,
    pub max_bounces: usize,
    pub background: Vec3,
}

impl Settings {
    pub fn builder() -> SettingsBuilder {
        SettingsBuilder {
            width: None,
            height: None,
            aspect_ratio: None,
            samples: None,
            max_bounces: None,
            background: None,
        }
    }
}

pub struct SettingsBuilder {
    width: Option<usize>,
    height: Option<usize>,
    aspect_ratio: Option<f32>,
    samples: Option<NonZeroUsize>,
    max_bounces: Option<usize>,
    background: Option<Vec3>,
}

#[allow(dead_code)]
impl SettingsBuilder {
    pub fn width(&mut self, p: usize) -> &mut Self {
        self.width = Some(p);
        self
    }

    pub fn height(&mut self, p: usize) -> &mut Self {
        self.height = Some(p);
        self
    }

    pub fn aspect_ratio(&mut self, p: f32) -> &mut Self {
        self.aspect_ratio = Some(p);
        self
    }

    pub fn samples(&mut self, p: NonZeroUsize) -> &mut Self {
        self.samples = Some(p);
        self
    }

    pub fn max_bounces(&mut self, p: usize) -> &mut Self {
        self.max_bounces = Some(p);
        self
    }

    pub fn background(&mut self, background: Vec3) -> &mut Self {
        self.background = Some(background);
        self
    }

    pub fn build(&self) -> Result<Settings, String> {
        let samples = match self.samples {
            Some(s) => s,
            None => return Err("Error in StringBuilder: You have to supply `samples`".to_owned()),
        };

        let max_bounces = match self.max_bounces {
            Some(s) => s,
            None => {
                return Err("Error in StringBuilder: You have to supply `max_bounces`".to_owned())
            }
        };

        let (width, height, aspect_ratio) = match (self.width, self.height, self.aspect_ratio) {
            (Some(w), Some(h), None) => (w, h, w as f32 / h as f32),
            (Some(w), None, Some(ar)) => (w, (w as f32 / ar) as usize, ar),
            (None, Some(h), Some(ar)) => ((h as f32 * ar) as usize, h, ar),
            _ => return Err("Invalid combination of `width`, `height`, and `aspect_ratio`. You have to supply exactly 2 of these fields".to_owned()),
        };

        let background = match self.background {
            Some(b) => b,
            None => Vec3::new(0.7, 0.8, 1.0),
        };

        Ok(Settings {
            width,
            height,
            aspect_ratio,
            samples,
            max_bounces,
            background,
        })
    }
}
