use super::*;

mod settings;
use settings::*;

use std::num::NonZeroUsize;
use std::sync::{Arc, Mutex};
use std::thread;

pub struct Scene {
    world: Arc<dyn Hittable>,
    camera: Arc<Camera>,
    pix_done: Arc<Mutex<usize>>,
    settings: Settings,
    num_threads: Option<NonZeroUsize>,
    progress_arc: Option<Arc<Mutex<f32>>>,
}

impl Scene {
    pub fn new(
        world: Arc<dyn Hittable>,
        camera: Camera,
        settings: Settings,
        progress_arc: Option<Arc<Mutex<f32>>>,
    ) -> Self {
        Self {
            world,
            camera: Arc::new(camera),
            pix_done: Arc::new(Mutex::new(0)),
            settings,
            num_threads: None,
            progress_arc,
        }
    }

    fn ray_color(r: &Ray, background: &Vec3, world: Arc<dyn Hittable>, depth: usize) -> Vec3 {
        if depth == 0 {
            return Vec3::zero();
        }
        let hit = world.hit(r, 0.001, f64::INFINITY);
        if hit.is_none() {
            return *background;
        }
        let rec = hit.unwrap();

        let mut scattered = Ray::default();
        let mut attenuation = Vec3::zero();
        let mut emitted = Vec3::zero();
        if let Some(mat) = rec.material.clone() {
            emitted = mat.emitted(rec.u, rec.v, &rec.point);
            if !mat.scatter(r, &rec, &mut attenuation, &mut scattered) {
                return emitted;
            }
        }
        return emitted + attenuation * Scene::ray_color(&scattered, &background, world, depth - 1);
    }

    #[allow(dead_code)]
    pub fn progress(&self) -> f32 {
        if let Some(num_threads) = self.num_threads {
            *self.pix_done.lock().unwrap() as f32
                / (self.settings.width * self.settings.height * num_threads.get()) as f32
        } else {
            0.0
        }
    }

    pub fn render(&mut self) -> Result<Vec<Vec3>, std::io::Error> {
        let pix_colors = Arc::new(Mutex::new(Vec::<Vec3>::with_capacity(
            self.settings.width * self.settings.height,
        )));
        for _ in 0..(self.settings.width * self.settings.height) {
            pix_colors.lock().unwrap().push(Vec3::zero());
        }

        self.num_threads =
            Some(thread::available_parallelism().unwrap_or(NonZeroUsize::new(2).unwrap()));
        let num_threads = self.num_threads.unwrap().get();
        let samples_per_pixel_per_thread = self.settings.samples.get() / num_threads;

        println!(
            "Starting render with:
  {} threads
  {} total number of samples per pixel
  {}x{} pixels
",
            num_threads,
            samples_per_pixel_per_thread * num_threads,
            self.settings.width,
            self.settings.height
        );

        let mut handles = Vec::with_capacity(num_threads);

        for _ in 0..num_threads {
            let world = self.world.clone();
            let pix_colors = pix_colors.clone();
            let camera = self.camera.clone();
            let pix_done_count = self.pix_done.clone();
            let samples_per_pixel_per_thread = samples_per_pixel_per_thread;
            let settings = self.settings;
            let progress_arc = self.progress_arc.clone();
            let num_threads = num_threads;
            let background = self.settings.background;
            handles.push(thread::spawn(move || {
                for y in 0..settings.height {
                    for x in 0..settings.width {
                        let mut pixel_color = Vec3::zero();
                        for _ in 0..samples_per_pixel_per_thread {
                            let u = (x as f64 + rand_norm()) / (settings.width as f64 - 1.0);
                            let v = (y as f64 + rand_norm()) / (settings.height as f64 - 1.0);
                            let r = camera.get_ray(u, v);
                            pixel_color += Scene::ray_color(
                                &r,
                                &background,
                                world.clone(),
                                settings.max_bounces,
                            );
                        }

                        pix_colors.lock().unwrap()
                            [x + settings.width * (settings.height - 1 - y)] += pixel_color;
                        *pix_done_count.lock().unwrap() += 1;
                        if let Some(progress_arc) = progress_arc.clone() {
                            *progress_arc.lock().unwrap() = *pix_done_count.lock().unwrap() as f32
                                / (settings.width * settings.height * num_threads) as f32;
                        }
                    }
                }
            }));
        }

        for handle in handles {
            if let Err(_) = handle.join() {
                return Err(std::io::Error::new(
                    std::io::ErrorKind::Other,
                    "Thread panicked",
                ));
            }
        }

        let pix_colors = &*pix_colors.lock().unwrap();

        Ok(pix_colors
            .iter()
            .map(|&c| correct_gamma(c / (samples_per_pixel_per_thread * num_threads) as f64))
            .collect())
    }

    #[allow(dead_code)]
    pub fn cornell_scene(
        samples: NonZeroUsize,
        max_bounces: usize,
        progress_arc: Option<Arc<Mutex<f32>>>,
    ) -> Self {
        let ar = 1.0;
        let camera = Camera::new(
            Vec3::new(277.5, 277.5, -800.0),
            Vec3::new(277.5, 277.5, 0.0),
            Vec3::new(0.0, 1.0, 0.0),
            40.0,
            ar,
            0.0,
            1.0,
            0.0,
            0.0,
        );

        let mut world = object::HittableList::new();

        let mut objs: Vec<Arc<dyn Hittable>> = Vec::new();

        let red = Arc::new(material::Lambertian::new(Vec3::new(0.65, 0.05, 0.05)));
        let white = Arc::new(material::Lambertian::new(Vec3::new(0.73, 0.73, 0.73)));
        let green = Arc::new(material::Lambertian::new(Vec3::new(0.12, 0.45, 0.15)));
        let light = Arc::new(material::DiffuseLight::new(Vec3::new(10.0, 10.0, 10.0)));

        // right
        objs.push(Arc::new(object::YZRect::new(
            Vec3::new(555.0, 277.5, 277.5),
            555.0,
            555.0,
            green.clone(),
        )));
        // left
        objs.push(Arc::new(object::YZRect::new(
            Vec3::new(0.0, 277.5, 277.5),
            555.0,
            555.0,
            red.clone(),
        )));

        // top
        objs.push(Arc::new(object::XZRect::new(
            Vec3::new(277.5, 555.0, 277.5),
            555.0,
            555.0,
            white.clone(),
        )));
        // bottom
        objs.push(Arc::new(object::XZRect::new(
            Vec3::new(277.5, 0.0, 277.5),
            555.0,
            555.0,
            white.clone(),
        )));

        // back
        objs.push(Arc::new(object::XYRect::new(
            Vec3::new(277.5, 277.5, 555.0),
            555.0,
            555.0,
            white.clone(),
        )));

        // light
        objs.push(Arc::new(object::XZRect::new(
            Vec3::new(277.5, 554.0, 277.5),
            220.0,
            220.0,
            light.clone(),
        )));

        let mut box1: Arc<dyn Hittable> = Arc::new(object::Block::new(
            Vec3::new(0.0, 0.0, 0.0),
            Vec3::new(165.0, 330.0, 165.0),
            white.clone(),
        ));

        box1 = Arc::new(object::Rotate::y(
            (15.0 / 180.0) * std::f64::consts::PI,
            box1,
        ));
        box1 = Arc::new(object::Translate::new(Vec3::new(350.0, 165.0, 350.0), box1));

        objs.push(box1);

        let mut box2: Arc<dyn Hittable> = Arc::new(object::Block::new(
            Vec3::new(0.0, 0.0, 0.0),
            Vec3::new(165.0, 165.0, 165.0),
            white.clone(),
        ));

        box2 = Arc::new(object::Rotate::y(
            (-18.0 / 180.0) * std::f64::consts::PI,
            box2,
        ));
        box2 = Arc::new(object::Translate::new(Vec3::new(180.0, 82.5, 170.0), box2));

        objs.push(box2);

        let count = objs.len();
        object::BvhNode::new(&mut objs, 0, count, 0.0, 0.0).insert(&mut world);

        Self::new(
            Arc::new(world),
            camera,
            Settings::builder()
                .width(400)
                .aspect_ratio(ar as f32)
                .samples(samples)
                .max_bounces(max_bounces)
                .background(Vec3::zero())
                .build()
                .unwrap(),
            progress_arc,
        )
    }

    #[allow(dead_code)]
    pub fn cornell_smoke(
        samples: NonZeroUsize,
        max_bounces: usize,
        progress_arc: Option<Arc<Mutex<f32>>>,
    ) -> Self {
        let ar = 1.0;
        let camera = Camera::new(
            Vec3::new(277.5, 277.5, -800.0),
            Vec3::new(277.5, 277.5, 0.0),
            Vec3::new(0.0, 1.0, 0.0),
            40.0,
            ar,
            0.0,
            1.0,
            0.0,
            0.0,
        );

        let mut world = object::HittableList::new();

        let mut objs: Vec<Arc<dyn Hittable>> = Vec::new();

        let red = Arc::new(material::Lambertian::new(Vec3::new(0.65, 0.05, 0.05)));
        let white = Arc::new(material::Lambertian::new(Vec3::new(0.73, 0.73, 0.73)));
        let green = Arc::new(material::Lambertian::new(Vec3::new(0.12, 0.45, 0.15)));
        let light = Arc::new(material::DiffuseLight::new(Vec3::new(10.0, 10.0, 10.0)));

        // right
        objs.push(Arc::new(object::YZRect::new(
            Vec3::new(555.0, 277.5, 277.5),
            555.0,
            555.0,
            green.clone(),
        )));
        // left
        objs.push(Arc::new(object::YZRect::new(
            Vec3::new(0.0, 277.5, 277.5),
            555.0,
            555.0,
            red.clone(),
        )));

        // top
        objs.push(Arc::new(object::XZRect::new(
            Vec3::new(277.5, 555.0, 277.5),
            555.0,
            555.0,
            white.clone(),
        )));
        // bottom
        objs.push(Arc::new(object::XZRect::new(
            Vec3::new(277.5, 0.0, 277.5),
            555.0,
            555.0,
            white.clone(),
        )));

        // back
        objs.push(Arc::new(object::XYRect::new(
            Vec3::new(277.5, 277.5, 555.0),
            555.0,
            555.0,
            white.clone(),
        )));

        // light
        objs.push(Arc::new(object::XZRect::new(
            Vec3::new(277.5, 554.0, 277.5),
            220.0,
            220.0,
            light.clone(),
        )));

        let mut box1: Arc<dyn Hittable> = Arc::new(object::Block::new(
            Vec3::new(0.0, 0.0, 0.0),
            Vec3::new(165.0, 330.0, 165.0),
            white.clone(),
        ));

        box1 = Arc::new(object::Rotate::y(
            (15.0 / 180.0) * std::f64::consts::PI,
            box1,
        ));
        box1 = Arc::new(object::Translate::new(Vec3::new(350.0, 165.0, 350.0), box1));

        objs.push(Arc::new(object::ConstantMedium::new(
            box1,
            Vec3::zero(),
            0.01,
        )));

        let mut box2: Arc<dyn Hittable> = Arc::new(object::Block::new(
            Vec3::new(0.0, 0.0, 0.0),
            Vec3::new(165.0, 165.0, 165.0),
            white.clone(),
        ));

        box2 = Arc::new(object::Rotate::y(
            (-18.0 / 180.0) * std::f64::consts::PI,
            box2,
        ));
        box2 = Arc::new(object::Translate::new(Vec3::new(180.0, 82.5, 170.0), box2));

        objs.push(Arc::new(object::ConstantMedium::new(
            box2,
            Vec3::new(1.0, 1.0, 1.0),
            0.01,
        )));

        let count = objs.len();
        object::BvhNode::new(&mut objs, 0, count, 0.0, 0.0).insert(&mut world);

        Self::new(
            Arc::new(world),
            camera,
            Settings::builder()
                .width(400)
                .aspect_ratio(ar as f32)
                .samples(samples)
                .max_bounces(max_bounces)
                .background(Vec3::zero())
                .build()
                .unwrap(),
            progress_arc,
        )
    }

    #[allow(dead_code)]
    pub fn test_scene(
        samples: NonZeroUsize,
        max_bounces: usize,
        progress_arc: Option<Arc<Mutex<f32>>>,
    ) -> Self {
        let ar = 3.0 / 2.0;
        let camera = Camera::new(
            Vec3::new(26.0, 3.0, 6.0),
            Vec3::new(0.0, 0.0, 0.0),
            Vec3::new(0.0, 1.0, 0.0),
            20.0,
            ar,
            0.0,
            1.0,
            0.0,
            0.0,
        );

        let mut world = object::HittableList::new();

        let tex = Arc::new(texture::noise::Noise::with_iterations(4.0, 10));
        let mat = Arc::new(material::Lambertian::from_texture(tex.clone()));

        object::Plane::new(
            Vec3::new(0.0, 0.0, 0.0),
            Vec3::new(0.0, 1.0, 0.0),
            mat.clone(),
        )
        .insert(&mut world);
        object::Sphere::new(Vec3::new(0.0, 2.0, 0.0), 2.0, mat.clone()).insert(&mut world);

        let light = Arc::new(material::DiffuseLight::new(Vec3::new(10.0, 10.0, 10.0)));
        object::XYRect::new(Vec3::new(4.0, 2.0, -2.0), 2.0, 2.0, light.clone()).insert(&mut world);

        Self::new(
            Arc::new(world),
            camera,
            Settings::builder()
                .width(400)
                .aspect_ratio(ar as f32)
                .samples(samples)
                .max_bounces(max_bounces)
                .background(Vec3::zero())
                .build()
                .unwrap(),
            progress_arc,
        )
    }

    #[allow(dead_code)]
    pub fn earth_scene(
        samples: NonZeroUsize,
        max_bounces: usize,
        progress_arc: Option<Arc<Mutex<f32>>>,
    ) -> Self {
        let ar = 3.0 / 2.0;
        let camera = Camera::new(
            Vec3::new(13.0, 2.0, 3.0),
            Vec3::new(0.0, 0.0, 0.0),
            Vec3::new(0.0, 1.0, 0.0),
            20.0,
            ar,
            0.0,
            1.0,
            0.0,
            0.0,
        );

        let mut world = object::HittableList::new();

        let tex = Arc::new(texture::Image::new("./assets/earthmap.jpg"));
        let mat = Arc::new(material::Lambertian::from_texture(tex.clone()));

        object::Sphere::new(Vec3::new(0.0, 0.0, 0.0), 2.0, mat.clone()).insert(&mut world);

        Self::new(
            Arc::new(world),
            camera,
            Settings::builder()
                .width(400)
                .aspect_ratio(ar as f32)
                .samples(samples)
                .max_bounces(max_bounces)
                .build()
                .unwrap(),
            progress_arc,
        )
    }

    #[allow(dead_code)]
    pub fn random_scene(
        samples: NonZeroUsize,
        max_bounces: usize,
        progress_arc: Option<Arc<Mutex<f32>>>,
    ) -> Self {
        // Camera
        let camera = {
            let lookfrom = Vec3::new(13.0, 2.0, 3.0);
            let lookat = Vec3::new(0.0, 0.0, 0.0);
            Camera::new(
                lookfrom,
                lookat,
                Vec3::new(0.0, 1.0, 0.0),
                20.0,
                3.0 / 2.0,
                0.1,
                10.0,
                0.0,
                1.0,
            )
        };

        // World
        let mut world = object::HittableList::new();

        let checker = Arc::new(texture::Checker::from_colors(
            Vec3::new(0.0, 0.0, 0.0),
            Vec3::new(0.9, 0.9, 0.9),
            5.0,
        ));
        let ground_material = Arc::new(material::DiffuseLight::from_texture(checker.clone()));
        object::Plane::new(
            Vec3::new(0.0, 0.0, 0.0),
            Vec3::new(0.0, 1.0, 0.0),
            ground_material.clone(),
        )
        .insert(&mut world);

        let mut objs = Vec::<Arc<dyn Hittable>>::new();
        for a in -11..11 {
            for b in -11..11 {
                let choose_mat = rand_norm();
                let center = Vec3::new(
                    a as f64 + 0.9 * rand_norm(),
                    0.2,
                    b as f64 + 0.9 * rand_norm(),
                );

                if (center - Vec3::new(4.0, 0.2, 0.0)).length() > 0.9 {
                    let object_material: Arc<dyn Material> = if choose_mat < 0.7 {
                        // diffuse
                        let albedo = Vec3::random() * Vec3::random();
                        Arc::new(material::Lambertian::new(albedo))
                    } else if choose_mat < 0.85 {
                        // metal
                        let albedo = Vec3::random_range(0.5, 1.0);
                        let fuzz = rand_range(0.0, 0.5);
                        Arc::new(material::Metal::new(albedo, fuzz))
                    } else
                    // if choose_mat < 0.95 {
                    //     // diffuse light
                    //     Arc::new(material::DiffuseLight::new(Vec3::random_range(0.25, 0.5)))
                    // } else
                    {
                        // glass
                        Arc::new(material::Dielectric::new(1.5))
                    };
                    let choose_obj = rand_norm();
                    if choose_obj < 0.4 {
                        objs.push(Arc::new(object::Sphere::new(
                            center,
                            0.2,
                            object_material.clone(),
                        )));
                    } else if choose_obj < 0.7 {
                        let center2 = center + Vec3::new(0.0, rand_range(0.0, 0.5), 0.0);

                        objs.push(Arc::new(object::MovingSphere::new(
                            center,
                            center2,
                            0.0,
                            1.0,
                            0.2,
                            object_material.clone(),
                        )));
                    } else {
                        objs.push(Arc::new(object::Cylinder::new(
                            center,
                            Vec3::new(0.0, 1.0, 0.0),
                            0.15,
                            0.4,
                            object_material.clone(),
                        )));
                    }
                }
            }
        }

        let material1 = Arc::new(material::Dielectric::new(1.5));
        objs.push(Arc::new(object::Sphere::new(
            Vec3::new(0.0, 1.0, 0.0),
            1.0,
            material1.clone(),
        )));

        let material2 = Arc::new(material::Lambertian::new(Vec3::new(0.4, 0.2, 0.1)));
        objs.push(Arc::new(object::Sphere::new(
            Vec3::new(-4.0, 1.0, 0.0),
            1.0,
            material2.clone(),
        )));

        let material3 = Arc::new(material::Metal::new(Vec3::new(0.7, 0.6, 0.5), 0.0));
        objs.push(Arc::new(object::Sphere::new(
            Vec3::new(4.0, 1.0, 0.0),
            1.0,
            material3.clone(),
        )));
        let end = objs.len();
        object::BvhNode::new(&mut objs, 0, end, 0.0, 1.0).insert(&mut &mut world);

        Self::new(
            Arc::new(world),
            camera,
            Settings::builder()
                .width(1200)
                .aspect_ratio(3.0 / 2.0)
                .samples(samples)
                .max_bounces(max_bounces)
                .background(Vec3::zero())
                .build()
                .unwrap(),
            progress_arc,
        )
    }

    #[allow(dead_code)]
    pub fn final_scene(
        samples: NonZeroUsize,
        max_bounces: usize,
        progress_arc: Option<Arc<Mutex<f32>>>,
    ) -> Self {
        let ar = 1.0;
        let camera = Camera::new(
            Vec3::new(478.0, 278.0, -600.0),
            Vec3::new(278.0, 278.0, 0.0),
            Vec3::new(0.0, 1.0, 0.0),
            40.0,
            ar,
            0.0,
            1.0,
            0.0,
            1.0,
        );

        let mut world = object::HittableList::new();

        // Ground
        let ground = Arc::new(material::Lambertian::new(Vec3::new(0.48, 0.83, 0.53)));

        let mut boxes1: Vec<Arc<dyn Hittable>> = Vec::new();
        let boxes_per_side = 20;

        for i in 0..boxes_per_side {
            for j in 0..boxes_per_side {
                let w = 100.0;
                boxes1.push(Arc::new(object::Block::new(
                    Vec3::new(
                        -1000.0 + w / 2.0 + w * i as f64,
                        rand_range(0.0, 50.0),
                        -1000.0 + w / 2.0 + w * j as f64,
                    ),
                    w * Vec3::new(1.0, 1.0, 1.0),
                    ground.clone(),
                )));
            }
        }

        let count = boxes1.len();
        object::BvhNode::new(&mut boxes1, 0, count, 0.0, 1.0).insert(&mut world);

        // Light
        let light = Arc::new(material::DiffuseLight::new(Vec3::new(7.0, 7.0, 7.0)));
        object::XZRect::new(Vec3::new(273.0, 554.0, 279.5), 300.0, 265.0, light).insert(&mut world);

        // Moving Sphere
        let center0 = Vec3::new(400.0, 400.0, 200.0);
        let center1 = center0 + Vec3::new(30.0, 0.0, 0.0);
        let mat = Arc::new(material::Lambertian::new(Vec3::new(0.7, 0.3, 0.1)));
        object::MovingSphere::new(center0, center1, 0.0, 1.0, 50.0, mat).insert(&mut world);

        // Glass & Metal
        object::Sphere::new(
            Vec3::new(260.0, 150.0, 45.0),
            50.0,
            Arc::new(material::Dielectric::new(1.5)),
        )
        .insert(&mut world);
        object::Sphere::new(
            Vec3::new(0.0, 150.0, 145.0),
            50.0,
            Arc::new(material::Metal::new(Vec3::new(0.8, 0.8, 0.9), 1.0)),
        )
        .insert(&mut world);

        // Volumes
        let boundary = Arc::new(object::Sphere::new(
            Vec3::new(360.0, 150.0, 145.0),
            70.0,
            Arc::new(material::Dielectric::new(1.5)),
        ));
        world.add(boundary.clone());
        world.add(Arc::new(object::ConstantMedium::new(
            boundary.clone(),
            Vec3::new(0.2, 0.4, 0.9),
            0.2,
        )));
        let boundary = Arc::new(object::Sphere::new(
            Vec3::zero(),
            5000.0,
            Arc::new(material::Dielectric::new(1.5)),
        ));
        world.add(Arc::new(object::ConstantMedium::new(
            boundary.clone(),
            Vec3::new(1.0, 1.0, 1.0),
            0.0001,
        )));

        // Earth
        let e_mat = Arc::new(material::Lambertian::from_texture(Arc::new(
            texture::Image::new("./assets/earthmap.jpg"),
        )));
        world.add(Arc::new(object::Sphere::new(
            Vec3::new(400.0, 200.0, 400.0),
            100.0,
            e_mat,
        )));

        // Noise
        let noise = Arc::new(material::Lambertian::from_texture(Arc::new(
            texture::Noise::new(0.1),
        )));
        world.add(Arc::new(object::Sphere::new(
            Vec3::new(220.0, 280.0, 300.0),
            80.0,
            noise,
        )));

        let mut spheres: Vec<Arc<dyn Hittable>> = Vec::new();
        let white = Arc::new(material::Lambertian::new(Vec3::new(0.73, 0.73, 0.73)));
        let count = 1000;
        for _ in 0..count {
            spheres.push(Arc::new(object::Sphere::new(
                Vec3::random_range(0.0, 165.0),
                10.0,
                white.clone(),
            )));
        }
        world.add(Arc::new(object::Translate::new(
            Vec3::new(-100.0, 270.0, 395.0),
            Arc::new(object::Rotate::y(
                (15.0 / 180.0) * std::f64::consts::PI,
                Arc::new(object::BvhNode::new(&mut spheres, 0, count, 0.0, 1.0)),
            )),
        )));

        Self::new(
            Arc::new(world),
            camera,
            Settings::builder()
                .width(800)
                .aspect_ratio(ar as f32)
                .samples(samples)
                .max_bounces(max_bounces)
                .background(Vec3::zero())
                .build()
                .unwrap(),
            progress_arc,
        )
    }

    pub fn get_width(&self) -> usize {
        self.settings.width
    }

    pub fn get_height(&self) -> usize {
        self.settings.height
    }
}

fn correct_gamma(col: Vec3) -> Vec3 {
    Vec3::new(col.x().sqrt(), col.y().sqrt(), col.z().sqrt())
}
