use std::sync::Arc;

pub mod math_utils;
pub use math_utils::*;
pub mod ray;
pub use ray::*;
pub mod object;
pub use object::{HitRecord, Hittable};
pub mod camera;
pub use camera::*;
pub mod material;
pub use material::Material;
pub mod scene;
pub use scene::Scene;
pub mod aabb;
pub use aabb::*;
pub mod texture;
pub use texture::Texture;
