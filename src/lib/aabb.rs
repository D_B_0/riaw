use std::{cmp::Ordering, sync::Arc};

use super::*;

#[derive(Clone, Copy)]
#[allow(non_camel_case_types)]
pub struct AABB {
    pub minimum: Vec3,
    pub maximum: Vec3,
}

#[allow(dead_code)]
impl AABB {
    pub fn new(min: Vec3, max: Vec3) -> Self {
        Self {
            minimum: min,
            maximum: max,
        }
    }

    pub fn default() -> Self {
        Self {
            minimum: Vec3::zero(),
            maximum: Vec3::zero(),
        }
    }

    pub fn from_points(points: &[Vec3]) -> Self {
        let min_x = points.iter().map(Vec3::x).reduce(f64::min).unwrap();
        let max_x = points.iter().map(Vec3::x).reduce(f64::max).unwrap();
        let min_y = points.iter().map(Vec3::y).reduce(f64::min).unwrap();
        let max_y = points.iter().map(Vec3::y).reduce(f64::max).unwrap();
        let min_z = points.iter().map(Vec3::z).reduce(f64::min).unwrap();
        let max_z = points.iter().map(Vec3::z).reduce(f64::max).unwrap();

        AABB::new(
            Vec3::new(min_x, min_y, min_z),
            Vec3::new(max_x, max_y, max_z),
        )
    }

    pub fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> bool {
        for a in 0..3 {
            let inv_d = 1.0 / r.dir[a];
            let mut t0 = (self.minimum[a] - r.orig[a]) * inv_d;
            let mut t1 = (self.maximum[a] - r.orig[a]) * inv_d;
            if inv_d < 0.0 {
                (t0, t1) = (t1, t0);
            }

            let t_min = if t0 > t_min { t0 } else { t_min };
            let t_max = if t1 < t_max { t1 } else { t_max };

            if t_max <= t_min {
                return false;
            }
        }

        true
    }

    pub fn surrounding_box(b0: AABB, b1: AABB) -> AABB {
        let small = Vec3::new(
            f64::min(b0.minimum.x(), b1.minimum.x()),
            f64::min(b0.minimum.y(), b1.minimum.y()),
            f64::min(b0.minimum.z(), b1.minimum.z()),
        );
        let big = Vec3::new(
            f64::max(b0.maximum.x(), b1.maximum.x()),
            f64::max(b0.maximum.y(), b1.maximum.y()),
            f64::max(b0.maximum.z(), b1.maximum.z()),
        );

        AABB {
            minimum: small,
            maximum: big,
        }
    }

    pub fn compare(a: &Arc<dyn Hittable>, b: &Arc<dyn Hittable>, axis: i32) -> Ordering {
        let box_a = a
            .bounding_box(0.0, 0.0)
            .expect("No bounding box in `AABB::compare`");
        let box_b = b
            .bounding_box(0.0, 0.0)
            .expect("No bounding box in `AABB::compare`");

        if box_a.minimum[axis as usize] < box_b.minimum[axis as usize] {
            Ordering::Less
        } else {
            Ordering::Greater
        }
    }

    pub fn compare_x(a: &Arc<dyn Hittable>, b: &Arc<dyn Hittable>) -> Ordering {
        AABB::compare(a, b, 0)
    }

    pub fn compare_y(a: &Arc<dyn Hittable>, b: &Arc<dyn Hittable>) -> Ordering {
        AABB::compare(a, b, 1)
    }

    pub fn compare_z(a: &Arc<dyn Hittable>, b: &Arc<dyn Hittable>) -> Ordering {
        AABB::compare(a, b, 2)
    }
}
